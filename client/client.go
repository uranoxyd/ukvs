package client

import (
	"bytes"
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"gitlab.com/uranoxyd/ukvs"
)

func hashSha256(input []byte) []byte {
	h := sha256.New()
	h.Write(input)
	return h.Sum(nil)
}

type Key = ukvs.Key
type Value = ukvs.Value

type Client struct {
	url      string
	client   *http.Client
	hashKeys bool
}

func NewClient(url string, hashKeys bool) *Client {
	for strings.HasSuffix(url, "/") {
		url = url[0 : len(url)-1]
	}
	return &Client{
		url:      url,
		client:   &http.Client{},
		hashKeys: hashKeys,
	}
}

func (c *Client) getKeyURL(key Key) string {
	if c.hashKeys {
		key = Key(key.Hash())
	}
	return c.url + "/" + string(key)
}

func (c *Client) Has(key Key) (has bool, err error) {
	var req *http.Request
	if req, err = http.NewRequest("HEAD", c.getKeyURL(key), nil); err != nil {
		return
	}

	if response, e := c.client.Do(req); e == nil {
		return response.StatusCode == http.StatusOK, nil
	} else {
		return false, err
	}
}

func (c *Client) Peek(key Key) (value *Value, err error) {
	var req *http.Request
	if req, err = http.NewRequest("HEAD", c.getKeyURL(key), nil); err != nil {
		return
	}

	if response, e := c.client.Do(req); e == nil {
		if response.StatusCode == http.StatusOK {
			value = &Value{}
			value.ReadResponse(response, true)
		} else {
			err = fmt.Errorf("response status: %d - %s", response.StatusCode, response.Status)
			return
		}
	} else {
		err = e
	}
	return
}

func (c *Client) Get(key Key) (value *Value, err error) {
	if response, e := c.client.Get(c.getKeyURL(key)); e == nil {
		defer response.Body.Close()
		if response.StatusCode == http.StatusOK {
			value = &Value{}
			value.ReadResponse(response, false)
		} else {
			err = fmt.Errorf("response status: %d - %s", response.StatusCode, response.Status)
			return
		}
	} else {
		err = e
	}
	return
}
func (c *Client) Set(key Key, value []byte) (err error) {
	var req *http.Request
	if req, err = http.NewRequest("PUT", c.getKeyURL(key), bytes.NewBuffer(value)); err != nil {
		return
	}

	if res, e := c.client.Do(req); e == nil {
		defer res.Body.Close()
		if res.StatusCode != http.StatusOK {
			return fmt.Errorf("response status: %d - %s", res.StatusCode, res.Status)
		}
	} else {
		err = e
	}
	return
}
func (c *Client) Delete(key Key) (err error) {
	var req *http.Request
	if req, err = http.NewRequest("DELETE", c.getKeyURL(key), nil); err != nil {
		return
	}

	if res, e := c.client.Do(req); e == nil {
		defer res.Body.Close()
		if res.StatusCode != http.StatusOK {
			return fmt.Errorf("response status: %d - %s", res.StatusCode, res.Status)
		}
	} else {
		err = e
	}
	return
}

func (c *Client) Marshal(key Key, value any) error {
	if data, err := json.Marshal(value); err == nil {
		c.Set(key, data)
	} else {
		return err
	}
	return nil
}
func (c *Client) Unmarshal(key Key, value any) error {
	if value, err := c.Get(key); err == nil {
		return json.Unmarshal(value.Data, value)
	} else {
		return err
	}
}
