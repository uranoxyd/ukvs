package main

import (
	"fmt"

	"gitlab.com/uranoxyd/ukvs/client"
)

func main() {

	client := client.NewClient("http://127.0.0.1:8282/public", false)

	if err := client.Set("foo", []byte("foobar")); err != nil {
		panic(err)
	}

	if value, err := client.Get("foo"); err == nil {
		fmt.Printf("value: %v\n", string(value.Data))
	} else {
		panic(err)
	}
}
