// Copyright 2023 David Ewelt <uranoxyd@gmail.com>
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful, but
//   WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//   General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program. If not, see <http://www.gnu.org/licenses/>.

package ukvs

import (
	"encoding/binary"
	"fmt"
	"io"
	"os"
	"sort"

	"gitlab.com/uranoxyd/uconf"
)

//--
//-- FileStorageChunk
//--

type FileStorageChunk struct {
	key      string
	offset   uint32
	size     uint32
	orphaned bool
}

func (c *FileStorageChunk) String() string {
	return fmt.Sprintf("<chunk %s offset=%d, size=%d, orphaned=%v>", c.key, c.offset, c.size, c.orphaned)
}

//--
//-- FileStorage
//--

type FileStorage struct {
	configuration     *SharedStorageConfiguration
	path              string
	usedChunks        map[Key]*FileStorageChunk
	chunks            []*FileStorageChunk
	file              *os.File
	footerOffset      uint32
	writeZeroOnDelete bool
}

func NewFileStorage() *FileStorage {
	return &FileStorage{
		usedChunks:    make(map[Key]*FileStorageChunk),
		configuration: &SharedStorageConfiguration{},
	}
}

func (s *FileStorage) loadConfig(instruction *uconf.Instruction) (err error) {
	if instruction.SubScope() == nil {
		return fmt.Errorf("missing config")
	}

	s.configuration.loadConfig(instruction)

	scope := instruction.SubScope()

	for _, v := range scope.Instructions() {
		switch v.Name() {
		case "path":
			if len(v.Arguments()) < 1 {
				break
			}
			s.path = v.Argument(0).String()
		case "write-zero-on-delete":
			if len(v.Arguments()) < 1 {
				break
			}
			if s.writeZeroOnDelete, err = v.Argument(0).AsBool(); err != nil {
				return err
			}
		}
	}

	return nil
}

func (s *FileStorage) Config() *SharedStorageConfiguration {
	return s.configuration
}

func (c *FileStorage) Open() error {
	logDebugf("opening filesystem storage: %s", c.path)
	file, err := os.OpenFile(c.path, os.O_RDWR|os.O_CREATE, 0750)
	if err != nil {
		return err
	}
	c.file = file

	if c.currentEnd() > 0 {
		c.readHeader()
		c.readFooter()
	} else {
		//-- new file
		c.footerOffset = 4
		c.writeHeader()
	}

	return nil
}

func (c *FileStorage) Close() error {
	c.writeFooter()

	return c.file.Close()
}

//--
//-- private functions

func (c *FileStorage) getChunkIndex(chunk *FileStorageChunk) int {
	for i, c := range c.chunks {
		if c == chunk {
			return i
		}
	}
	return -1
}
func (c *FileStorage) currentCursor() int {
	offset, _ := c.file.Seek(0, io.SeekCurrent)
	return int(offset)
}
func (c *FileStorage) currentEnd() int {
	offset, _ := c.file.Seek(0, io.SeekEnd)
	return int(offset)
}

func (c *FileStorage) combineOrphanedChunks() {
	current := 0
	for {
		next := current + 1

		//-- whe have no next
		if next >= len(c.chunks) {
			break
		}

		cc := c.chunks[current]
		nc := c.chunks[next]

		if cc.orphaned && nc.orphaned {
			cc.size += nc.size
			c.chunks = sliceRemoveAt(c.chunks, current+1)
		} else {
			current++
		}
	}
}

func (c *FileStorage) findBestFittingOrphan(value *Value) *FileStorageChunk {
	//-- find the best fitting orphan :)
	var orphans []*FileStorageChunk
	for _, chunk := range c.chunks {
		if chunk.orphaned && chunk.size >= value.BinarySize() {
			orphans = append(orphans, chunk)
		}
	}
	if len(orphans) > 0 {
		sort.SliceStable(orphans, func(i, j int) bool {
			return orphans[i].size < orphans[j].size
		})
		return orphans[0]
	}

	return nil
}

func (c *FileStorage) registerAndWriteChunkValue(chunk *FileStorageChunk, key Key, value *Value) error {
	c.chunks = append(c.chunks, chunk)
	c.usedChunks[key] = chunk

	c.file.Seek(int64(chunk.offset), 0)
	value.Write(c.file)

	return nil
}

func (c *FileStorage) writeHeader() {
	c.file.Seek(0, io.SeekStart)
	binary.Write(c.file, Endianess, c.footerOffset)
}

func (c *FileStorage) readHeader() {
	c.file.Seek(0, io.SeekStart)
	binary.Read(c.file, Endianess, &c.footerOffset)
}

func (c *FileStorage) writeFooter() {
	c.file.Seek(int64(c.footerOffset), io.SeekStart)

	binary.Write(c.file, Endianess, int32(len(c.chunks)))
	for _, fsc := range c.chunks {
		writeString(c.file, fsc.key)
		binary.Write(c.file, Endianess, fsc.offset)
		binary.Write(c.file, Endianess, fsc.size)
		binary.Write(c.file, Endianess, fsc.orphaned)
	}

	c.writeHeader()
}

func (c *FileStorage) readFooter() {
	c.file.Seek(int64(c.footerOffset), 0)

	var numChunks int32
	binary.Read(c.file, Endianess, &numChunks)
	for i := 0; i < int(numChunks); i++ {
		chunk := &FileStorageChunk{}
		readString(c.file, &chunk.key)
		binary.Read(c.file, Endianess, &chunk.offset)
		binary.Read(c.file, Endianess, &chunk.size)
		binary.Read(c.file, Endianess, &chunk.orphaned)

		c.chunks = append(c.chunks, chunk)
		if !chunk.orphaned {
			c.usedChunks[Key(chunk.key)] = chunk
		}
	}
}

func (c *FileStorage) createAndWriteNewChunk(key Key, value *Value) {
	orphan := c.findBestFittingOrphan(value)
	if orphan != nil {
		oldOrphanSize := orphan.size
		orphan.orphaned = false
		orphan.size = value.BinarySize()
		orphan.key = string(key)

		c.file.Seek(int64(orphan.offset), 0)
		value.Write(c.file)

		//-- check if have some leftover space and create a new orphan chunk if so
		if orphan.size < oldOrphanSize {
			newOrphanedChunk := &FileStorageChunk{
				orphaned: true,
				offset:   orphan.offset + orphan.size,
				size:     oldOrphanSize - orphan.size,
			}
			c.chunks = sliceInsertAt(c.chunks, c.getChunkIndex(orphan)+1, newOrphanedChunk)
		}

		return
	}

	chunk := &FileStorageChunk{
		key:    string(key),
		offset: c.footerOffset,
		size:   value.BinarySize(),
	}
	c.footerOffset += value.BinarySize()

	c.registerAndWriteChunkValue(chunk, key, value)

	c.writeFooter()
}

//--
//-- Storage interface implementation

func (c *FileStorage) Start() error {
	logInfo("[FilesystemStorage] start")
	return c.Open()
}
func (c *FileStorage) Stop() error {
	logInfo("[FilesystemStorage] stop")
	return c.Close()
}
func (c *FileStorage) KeyCount() int {
	km := make(Keymap)
	c.CollectKeys(km)
	return len(km)
}
func (c *FileStorage) Keys() (keys []Key) {
	km := make(Keymap)
	c.CollectKeys(km)
	for k := range km {
		keys = append(keys, k)
	}
	return
}
func (c *FileStorage) CollectKeys(keymap Keymap) {
	for k := range c.usedChunks {
		keymap[k] = true
	}
}
func (c *FileStorage) Has(key Key) bool {
	return c.usedChunks[key] != nil
}
func (c *FileStorage) Head(key Key) (*Value, error) {
	if chunk, found := c.usedChunks[key]; found {
		c.file.Seek(int64(chunk.offset), io.SeekStart)
		value := &Value{}
		value.ValueHeader.Read(c.file)

		return value, nil
	}
	return nil, nil
}
func (c *FileStorage) Get(key Key) (*Value, error) {
	if chunk, found := c.usedChunks[key]; found {
		c.file.Seek(int64(chunk.offset), io.SeekStart)
		value := &Value{}
		value.Read(c.file)

		return value, nil
	}
	return nil, nil
}
func (s *FileStorage) SetHead(key Key, value *Value) error {
	if chunk, found := s.usedChunks[key]; found {
		s.file.Seek(int64(chunk.offset), io.SeekStart)
		value.ValueHeader.Write(s.file)
	}
	return nil
}
func (s *FileStorage) Set(key Key, value *Value) error {
	if chunk, found := s.usedChunks[key]; found {
		if value.BinarySize() > chunk.size {
			//-- the new data is larger then the old one,
			//-- we need to find a orphaned chunk or create a new one
			orphan := s.findBestFittingOrphan(value)
			if orphan != nil {
				oldOrphanSize := orphan.size
				orphan.orphaned = false
				orphan.size = value.BinarySize()
				orphan.key = string(key)

				s.file.Seek(int64(orphan.offset), 0)
				value.Write(s.file)

				//-- check if have some leftover space and create a new orphan chunk if so
				if orphan.size < oldOrphanSize {
					newOrphanedChunk := &FileStorageChunk{
						orphaned: true,
						offset:   orphan.offset + orphan.size,
						size:     oldOrphanSize - orphan.size,
					}
					s.chunks = sliceInsertAt(s.chunks, s.getChunkIndex(orphan)+1, newOrphanedChunk)
				}
			} else {
				chunk := &FileStorageChunk{
					key:    string(key),
					offset: s.footerOffset,
					size:   value.BinarySize(),
				}
				s.footerOffset += value.BinarySize()
				s.registerAndWriteChunkValue(chunk, key, value)
				s.writeFooter()
			}
		} else if value.BinarySize() <= chunk.size {
			//-- the new data is smaller then the old one,
			//-- in every case we need to write the data
			oldChunkSize := chunk.size
			s.file.Seek(int64(chunk.offset), 0)
			value.Write(s.file)
			chunk.size = value.BinarySize()

			//-- check if there is some leftover space and if so, create a new orphan chunk
			if chunk.size < oldChunkSize {
				newOrphanedChunk := &FileStorageChunk{
					orphaned: true,
					offset:   chunk.offset + chunk.size,
					size:     oldChunkSize - chunk.size,
				}
				s.chunks = sliceInsertAt(s.chunks, s.getChunkIndex(chunk)+1, newOrphanedChunk)
			}
		}
	} else {
		s.createAndWriteNewChunk(key, value)
	}

	return nil
}
func (s *FileStorage) Delete(key Key) error {
	if chunk, found := s.usedChunks[key]; found {
		delete(s.usedChunks, key)
		chunk.orphaned = true
		chunk.key = ""

		if s.writeZeroOnDelete {
			s.file.Seek(int64(chunk.offset), 0)
			fill := make([]byte, chunk.size)
			s.file.Write(fill)
		}

		s.combineOrphanedChunks()

	} else {
		fmt.Println("chunk not found")
	}
	return nil
}
func (c *FileStorage) Size() int64 {
	return int64(c.currentEnd())
}
