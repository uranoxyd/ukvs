package ukvs

import (
	"net"
	"net/http"
)

type ServerMetrics struct {
	Requests           uint64
	RequestsPerMethod  map[string]uint64
	RequestStatusCodes map[int]uint64
}

func NewServerMetrics() *ServerMetrics {
	return &ServerMetrics{
		RequestsPerMethod:  make(map[string]uint64),
		RequestStatusCodes: make(map[int]uint64),
	}
}

func (m *ServerMetrics) CountRequest(req *http.Request) {
	m.RequestsPerMethod[req.Method]++
	m.Requests++
}
func (m *ServerMetrics) CountStatus(code int) {
	m.RequestStatusCodes[code]++
}

type IPNetMap map[*net.IPNet]bool

func (m IPNetMap) Contains(ip net.IP) bool {
	for n := range m {
		if n.Contains(ip) {
			return true
		}
	}
	return false
}
