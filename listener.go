// Copyright 2023 David Ewelt <uranoxyd@gmail.com>
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful, but
//   WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//   General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program. If not, see <http://www.gnu.org/licenses/>.

package ukvs

import (
	"fmt"
	"net"
	"strconv"
	"strings"
)

const DEFAULT_LISTEN_PORT = 48282

type Listener struct {
	ip   net.IP
	port int
}

func NewListener() *Listener {
	return &Listener{}
}

func (l *Listener) ParseAddress(address string) (err error) {
	l.port = DEFAULT_LISTEN_PORT

	if idx := strings.LastIndex(address, ":"); idx > -1 {
		portString := address[idx+1:]
		address = address[:idx]
		if port, err := strconv.ParseInt(portString, 10, 32); err == nil {
			l.port = int(port)
		} else {
			return fmt.Errorf("invalid port: %s", portString)
		}
	}

	if len(address) >= 2 && address[0] == '[' && address[len(address)-1] == ']' {
		address = address[1 : len(address)-1]
	}

	l.ip = net.ParseIP(address)
	if l.ip == nil {
		return fmt.Errorf("invalid ip address: %s", address)
	}

	return
}

func (l *Listener) Address() string {
	if l.ip.To4() == nil { //-- check if ipv6
		return fmt.Sprintf("[%s]:%d", l.ip, l.port)
	} else {
		return fmt.Sprintf("%s:%d", l.ip, l.port)
	}
}
