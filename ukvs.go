// Copyright 2023 David Ewelt <uranoxyd@gmail.com>
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful, but
//   WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//   General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program. If not, see <http://www.gnu.org/licenses/>.

package ukvs

import (
	"crypto/sha1"
	"encoding/binary"
	"encoding/hex"
	"io"
	"time"
)

var Endianess = binary.BigEndian

// type ByteSize bytesize.ByteSize

// func (s ByteSize) MarshalYAML() (interface{}, error) {
// 	// yaml.Marshaler
// 	return string() , nil
// }

type Key string

func (k Key) Hash() string {
	h := sha1.New()
	h.Write([]byte(k))
	return hex.EncodeToString(h.Sum(nil))
}

func readTime(r io.Reader, target *time.Time) error {
	var millis int64
	if err := binary.Read(r, Endianess, &millis); err == nil {
		*target = time.UnixMilli(millis)
	} else {
		return err
	}
	return nil
}
