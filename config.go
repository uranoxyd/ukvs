// Copyright 2023 David Ewelt <uranoxyd@gmail.com>
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful, but
//   WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//   General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program. If not, see <http://www.gnu.org/licenses/>.

package ukvs

import (
	"strings"

	"gitlab.com/uranoxyd/uconf"
)

//--
//-- ConfigStringSet
//--

type ConfigStringSet map[string]bool

func (m ConfigStringSet) Keys() (keys []string) {
	for k := range m {
		keys = append(keys, k)
	}
	return
}

func (m ConfigStringSet) Contains(key string) bool {
	return m[strings.ToLower(key)]
}
func (m ConfigStringSet) Set(key string, value bool) {
	m[strings.ToUpper(key)] = value
}
func (m ConfigStringSet) Get(key string) bool {
	return m[strings.ToLower(key)]
}
func (m ConfigStringSet) Delete(key string) {
	delete(m, strings.ToLower(key))
}
func (m ConfigStringSet) String() string {
	return strings.Join(m.Keys(), ",")
}

func (m *ConfigStringSet) loadConfig(instruction *uconf.Instruction) error {
	for _, arg := range instruction.Arguments() {
		value := strings.ToLower(arg.String())
		operation := value[0]
		var flag string
		if operation != '+' && operation != '-' {
			operation = '+'
			flag = value
		} else {
			flag = value[1:]
		}
		switch operation {
		case '=':
			*m = make(map[string]bool)
			(*m)[flag] = true
		case '+':
			(*m)[flag] = true
		case '-':
			delete(*m, flag)
		}
	}
	return nil
}
