// Copyright 2023 David Ewelt <uranoxyd@gmail.com>
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful, but
//   WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//   General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program. If not, see <http://www.gnu.org/licenses/>.

package ukvs

import (
	"time"

	"gitlab.com/uranoxyd/uconf"
)

type StorageChain struct {
	storages []Storage
}

func (s *StorageChain) Config() *SharedStorageConfiguration {
	return nil
}

func (s *StorageChain) loadConfig(instruction *uconf.Instruction) error {
	if instruction.SubScope() == nil {
		return nil
	}

	for _, chainConfig := range instruction.SubScope().Instructions() {
		chainName := chainConfig.Name()

		var storage Storage
		switch chainName {
		case "memory":
			mStorage := NewMemoryStorage()
			if err := mStorage.loadConfig(chainConfig); err != nil {
				return err
			}
			storage = mStorage

		case "file":
			fsStorage := NewFileStorage()
			if err := fsStorage.loadConfig(chainConfig); err != nil {
				return err
			}
			storage = fsStorage
		}

		if storage != nil {
			s.storages = append(s.storages, storage)
		}
	}

	logDebugf("loaded %d storages", len(s.storages))

	return nil
}

func NewStorageChain() *StorageChain {
	return &StorageChain{}
}

func (c *StorageChain) Storages() []Storage {
	return c.storages
}

func (c *StorageChain) Start() error {
	logInfo("[StorageChain] start")
	for _, s := range c.storages {
		if err := s.Start(); err != nil {
			return err
		}
	}
	return nil
}
func (c *StorageChain) Stop() error {
	logInfo("[StorageChain] stop")

	for _, s := range c.storages {
		if err := s.Stop(); err != nil {
			return err
		}
	}

	return nil
}
func (c *StorageChain) KeyCount() int {
	km := make(Keymap)
	c.CollectKeys(km)
	return len(km)
}
func (c *StorageChain) Keys() (keys []Key) {
	km := make(Keymap)
	c.CollectKeys(km)
	for k := range km {
		keys = append(keys, k)
	}
	return
}
func (c *StorageChain) CollectKeys(keymap Keymap) {
	for _, s := range c.storages {
		s.CollectKeys(keymap)
	}
}
func (c *StorageChain) Has(key Key) bool {
	for _, s := range c.storages {
		if s.Has(key) {
			return true
		}
	}
	return false
}
func (c *StorageChain) Head(key Key) (*Value, error) {
	return c.Get(key)
}
func (c *StorageChain) Get(key Key) (value *Value, err error) {
	var storage Storage
	for _, storage = range c.storages {
		if storage.Has(key) {
			value, err = storage.Get(key)
			if err != nil {
				return
			} else if value == nil {
				continue
			}

			cfg := storage.Config()
			if cfg.deleteAfter > 0 {
				if value.LastRead.IsZero() {
					if time.Since(value.LastWrite) > cfg.deleteAfter {
						logDebugf("lazy delete key %s", key)
						storage.Delete(key)
						return nil, nil
					}
				} else {
					if time.Since(value.LastRead) > cfg.deleteAfter {
						logDebugf("lazy delete key %s", key)
						storage.Delete(key)
						return nil, nil
					}
				}
			}

			return
		}
	}

	return
}
func (c *StorageChain) SetHead(key Key, value *Value) error {
	for _, storage := range c.storages {
		if err := storage.SetHead(key, value); err != nil {
			return err
		}
	}
	return nil
}
func (c *StorageChain) Set(key Key, value *Value) error {
	for _, storage := range c.storages {
		if err := storage.Set(key, value); err != nil {
			return err
		}
	}
	return nil
}
func (c *StorageChain) Delete(key Key) error {
	for _, s := range c.storages {
		s.Delete(key)
	}
	return nil
}
func (c *StorageChain) Size() (size int64) {
	for _, s := range c.storages {
		size += s.Size()
	}
	return
}
