// Copyright 2023 David Ewelt <uranoxyd@gmail.com>
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful, but
//   WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//   General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program. If not, see <http://www.gnu.org/licenses/>.

package ukvs

import (
	"crypto/rand"
	"crypto/sha1"
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"hash/fnv"
	"io"
	"net"
	"unicode"
)

func writeString(w io.Writer, value string) error {
	stringBytes := []byte(value)
	if err := binary.Write(w, Endianess, uint32(len(stringBytes))); err != nil {
		return err
	}
	if _, err := w.Write(stringBytes); err != nil {
		return err
	}
	return nil
}
func readString(r io.Reader, value *string) error {
	var length uint32
	if err := binary.Read(r, Endianess, &length); err != nil {
		return err
	}
	var stringBytes []byte = make([]byte, length)
	if _, err := r.Read(stringBytes); err != nil {
		return err
	}
	*value = string(stringBytes)
	return nil
}

func PrintHexView(input []byte, bytesPerRow int) {
	out := ""
	var lineBytes []byte = make([]byte, bytesPerRow)
	for i, b := range input {
		lineBytes[i%bytesPerRow] = b
		if i > 0 && i%4 == 0 {
			out += " "
		}
		if i > 0 && i%bytesPerRow == 0 {
			out += " | "
			for _, lb := range lineBytes {
				r := rune(lb)
				if unicode.IsGraphic(r) || unicode.IsPrint(r) {
					out += string(r)
				} else {
					out += "."
				}
			}
			out += "\n"
		}
		out += fmt.Sprintf("%02x", b)
	}
	fmt.Println("-------- hex view -------- --------")
	fmt.Println(out)
	fmt.Println("-------- -------- -------- --------")
}

func hash32(value string) uint32 {
	h := fnv.New32a()
	h.Write([]byte(value))
	return h.Sum32()
}
func hash64(value string) uint64 {
	h := fnv.New64a()
	h.Write([]byte(value))
	return h.Sum64()
}

func hashSha1(value []byte) string {
	m := sha1.New()
	m.Write(value)
	return hex.EncodeToString(m.Sum(nil))
}

func randomHex(numBytes int) string {
	buffer := make([]byte, numBytes)
	rand.Read(buffer)
	return hex.EncodeToString(buffer)
}

func sliceRemoveAt[T any](slice []T, index int) []T {
	return append(slice[:index], slice[index+1:]...)
}
func sliceInsertAt[T any](slice []T, index int, value T) (result []T) {
	if len(slice) == index {
		return append(slice, value)
	}
	slice = append(slice[:index+1], slice[index:]...)
	slice[index] = value
	return slice
}
func sliceContains[T comparable](slice []T, item T) bool {
	for _, v := range slice {
		if v == item {
			return true
		}
	}
	return false
}

func getInterfaceByIP(ip net.IP) (iface net.Interface, err error) {
	var interfaces []net.Interface
	var addrs []net.Addr
	if interfaces, err = net.Interfaces(); err == nil {
		for _, iface = range interfaces {
			if addrs, err = iface.Addrs(); err == nil {
				for _, addr := range addrs {
					if ipnet, ok := addr.(*net.IPNet); ok {
						if ipnet.Contains(ip) {
							return iface, nil
						}
					}
				}
			}
		}
	} else {
		return
	}
	return
}

func writeLine(w io.Writer, line string) error {
	if _, err := w.Write([]byte(line + "\n")); err != nil {
		return err
	}
	return nil
}
