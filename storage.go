// Copyright 2023 David Ewelt <uranoxyd@gmail.com>
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful, but
//   WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//   General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program. If not, see <http://www.gnu.org/licenses/>.

package ukvs

type Keymap map[Key]bool

type KeySetForbiddenError struct {
	message string
}

func NewKeySetForbiddenError(message string) *KeySetForbiddenError {
	return &KeySetForbiddenError{message: message}
}
func (e *KeySetForbiddenError) Error() string {
	return e.message
}

type Storage interface {
	Config() *SharedStorageConfiguration
	Start() error
	Stop() error
	KeyCount() int
	CollectKeys(keymap Keymap)
	Keys() []Key
	Has(key Key) bool
	Head(key Key) (*Value, error)
	Get(key Key) (*Value, error)
	Set(key Key, value *Value) error
	SetHead(key Key, value *Value) error
	Delete(key Key) error
	Size() int64
}
