package ukvs

import "gitlab.com/uranoxyd/uconf"

const SeverAuthDefaultRealm = "Authentication required"

type ServerAuth struct {
	realm string
	users map[string]string
}

func NewServerAuth() *ServerAuth {
	return &ServerAuth{
		users: make(map[string]string),
	}
}

func (s *ServerAuth) loadConfig(instruction *uconf.Instruction) error {
	if instruction.NumArguments() > 0 {
		s.realm = instruction.Argument(0).String()
	} else {
		s.realm = SeverAuthDefaultRealm
	}

	scope := instruction.SubScope()
	if scope == nil {
		return nil
	}

	for _, i := range scope.Instructions() {
		s.users[i.Name()] = i.Argument(0).String()
	}

	return nil
}
