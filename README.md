# ukvs

Key-Value store with REST API

The state of this project is alpha. Dont expect any stability or consistency until version v1

# Quick-Start

1. Download and extract the latest release.
2. Rename ``config.example.conf`` to ``config.conf``
3. Run the server
	```bash
	$ ./ukvs
	```
	With the default configuration the server will listen on ``127.0.0.1:8282`` with a memory only storage.

4. Try some methods
	```bash
	$ curl -X PUT -d "Hello World" http://127.0.0.1:8282/hello
	$ curl http://127.0.0.1:8282/hello
	$ curl -X DELETE http://127.0.0.1:8282/hello
	```

# Configuration

The ``config.exmaple.conf`` contains relatively detailed information about all settings. More practical examples will follow.

In the default configuration, the values are set so that the server provides as little information as possible. This includes disabling the server banner, limiting error output, and disabling the output of a key's meta information.

# Methods

| Method     | Description
| ---------- | -----------
| ``PUT``    | set data
| ``GET``    | get data
| ``HEAD``   | get meta-info
| ``DELETE`` | delete


# Client

There is also a client available. See ``examples/client`` for usage.

# License

	GNU
