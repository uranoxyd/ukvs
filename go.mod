module gitlab.com/uranoxyd/ukvs

go 1.20

require (
	gitlab.com/uranoxyd/bytesize v0.1.2
	gitlab.com/uranoxyd/uconf v0.1.22
)

require gitlab.com/uranoxyd/uparse v0.2.1 // indirect
