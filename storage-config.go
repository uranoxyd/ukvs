// Copyright 2023 David Ewelt <uranoxyd@gmail.com>
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful, but
//   WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//   General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program. If not, see <http://www.gnu.org/licenses/>.

package ukvs

import (
	"time"

	"gitlab.com/uranoxyd/bytesize"
	"gitlab.com/uranoxyd/uconf"
)

type SharedStorageConfiguration struct {
	maxItems    int
	maxSize     bytesize.ByteSize
	maxItemSize bytesize.ByteSize
	writeOnce   bool
	deleteAfter time.Duration
}

func (s *SharedStorageConfiguration) loadConfig(instruction *uconf.Instruction) (err error) {
	scope := instruction.SubScope()
	if scope == nil {
		return
	}

	for _, v := range scope.Instructions() {
		args := v.Arguments()
		switch v.Name() {
		case "max-items":
			if len(args) < 1 {
				break
			}
			if s.maxItems, err = args[0].AsInt(); err != nil {
				return
			}

		case "max-size":
			if len(args) < 1 {
				break
			}
			if err = args[0].UnmarshalText(&s.maxSize); err != nil {
				return
			}

		case "max-item-size":
			if len(args) < 1 {
				break
			}
			if err = args[0].UnmarshalText(&s.maxItemSize); err != nil {
				return
			}

		case "write-once":
			if len(args) < 1 {
				break
			}
			if s.writeOnce, err = args[0].AsBool(); err != nil {
				return err
			}

		case "delete-after":
			if len(args) < 1 {
				break
			}
			if s.deleteAfter, err = args[0].AsDuration(); err != nil {
				return
			}
		}
	}

	return nil
}
