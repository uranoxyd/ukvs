// Copyright 2023 David Ewelt <uranoxyd@gmail.com>
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful, but
//   WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//   General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program. If not, see <http://www.gnu.org/licenses/>.

package ukvs

import (
	"fmt"
	"log"
	"sync"
)

const (
	LogLevelNone = 0

	LogLevelError = 10
	LogLevelWarn  = 20

	LogLevelInfo  = 100
	LogLevelDebug = 200
	LogLevelTrace = 200
)

var logLevel int = LogLevelNone
var logMutex sync.Mutex

func SetLogLevel(level int) {
	logLevel = level
}

func logError(v ...any) {
	logMutex.Lock()
	defer logMutex.Unlock()
	if logLevel < LogLevelError {
		return
	}
	log.Println("[ERROR]", fmt.Sprint(v...))
}
func logErrorf(format string, a ...any) {
	logMutex.Lock()
	defer logMutex.Unlock()
	if logLevel < LogLevelError {
		return
	}
	log.Println("[ERROR]", fmt.Sprintf(format, a...))
}

func logInfo(v ...any) {
	logMutex.Lock()
	defer logMutex.Unlock()
	if logLevel < LogLevelInfo {
		return
	}
	log.Println("[INFO ]", fmt.Sprint(v...))
}
func logInfof(format string, a ...any) {
	logMutex.Lock()
	defer logMutex.Unlock()
	if logLevel < LogLevelInfo {
		return
	}
	log.Println("[INFO ]", fmt.Sprintf(format, a...))
}

func logDebug(v ...any) {
	logMutex.Lock()
	defer logMutex.Unlock()
	if logLevel < LogLevelDebug {
		return
	}
	log.Println("[DEBUG]", fmt.Sprint(v...))
}
func logDebugf(format string, a ...any) {
	logMutex.Lock()
	defer logMutex.Unlock()
	if logLevel < LogLevelDebug {
		return
	}
	log.Println("[DEBUG]", fmt.Sprintf(format, a...))
}
func logTrace(v ...any) {
	logMutex.Lock()
	defer logMutex.Unlock()
	if logLevel < LogLevelTrace {
		return
	}
	log.Println("[TRACE]", fmt.Sprint(v...))
}
func logTracef(format string, a ...any) {
	logMutex.Lock()
	defer logMutex.Unlock()
	if logLevel < LogLevelTrace {
		return
	}
	log.Println("[TRACE]", fmt.Sprintf(format, a...))
}
