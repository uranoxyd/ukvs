#!/usr/bin/env python
import os, sys

current = "v0.1.0"
if os.path.exists("version"):
    with open("version", "r") as fp:
        current = fp.read()        
        
if len(sys.argv) >= 2:    
    pass
else:
    sys.stdout.write(current)

with open("version", "w") as fp:
    fp.write(current)
