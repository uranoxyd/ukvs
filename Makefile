clean:
	rm -rf builds

build-linux:
	./build-linux.sh

build: build-linux

run:
	cd app; go run .

tag:
	./version.py > /dev/null
	git tag -a -f `cat version` -m `cat version`
