// Copyright 2023 David Ewelt <uranoxyd@gmail.com>
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful, but
//   WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//   General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program. If not, see <http://www.gnu.org/licenses/>.

package ukvs

import (
	"encoding/binary"
	"io"
	"net/http"
	"strconv"
	"time"
)

const (
	ValueHeader_Etag      = 1 << iota
	ValueHeader_Created   = 2 << iota
	ValueHeader_LastRead  = 3 << iota
	ValueHeader_LastWrite = 4 << iota
	ValueHeader_Reads     = 5 << iota
	ValueHeader_Writes    = 6 << iota
)

const ValueHeaderFlag_ContentType = 1

const (
	CompressionZLib = 0
)

const ValueHeaderSize = 4 + 8 + 8 + 8 + 8 + 8 + 4

type ValueHeader struct {
	Flags     uint32
	Created   time.Time
	LastRead  time.Time
	LastWrite time.Time
	Reads     uint64
	Writes    uint64
	DataSize  uint32
}

func (s *ValueHeader) Read(r io.Reader) error {
	binary.Read(r, Endianess, &s.Flags)

	readTime(r, &s.Created)
	readTime(r, &s.LastRead)
	readTime(r, &s.LastWrite)

	binary.Read(r, Endianess, &s.Reads)
	binary.Read(r, Endianess, &s.Writes)

	binary.Read(r, Endianess, &s.DataSize)

	return nil
}
func (s *ValueHeader) Write(w io.Writer) error {
	binary.Write(w, Endianess, s.Flags)

	binary.Write(w, Endianess, int64(s.Created.UnixMilli()))
	binary.Write(w, Endianess, int64(s.LastRead.UnixMilli()))
	binary.Write(w, Endianess, int64(s.LastWrite.UnixMilli()))

	binary.Write(w, Endianess, s.Reads)
	binary.Write(w, Endianess, s.Writes)

	binary.Write(w, Endianess, s.DataSize)

	return nil
}

//--
//-- Value
//--

type Value struct {
	ValueHeader
	Data []byte
}

func NewValue(data []byte) *Value {
	return &Value{
		ValueHeader: ValueHeader{
			Created:   time.Now(),
			LastWrite: time.Now(),
			DataSize:  uint32(len(data)),
		},
		Data: data,
	}
}
func (v *Value) BinarySize() uint32 {
	return ValueHeaderSize + v.DataSize
}

func (s *Value) Write(w io.Writer) error {
	s.DataSize = uint32(len(s.Data))
	if err := s.ValueHeader.Write(w); err != nil {
		return err
	}

	w.Write(s.Data)

	return nil
}
func (s *Value) Read(r io.Reader) error {
	if err := s.ValueHeader.Read(r); err != nil {
		return err
	}

	s.Data = make([]byte, s.DataSize)
	r.Read(s.Data)

	return nil
}

func (value *Value) ReadResponse(response *http.Response, skipData bool) error {
	if v := response.Header.Get("Ukvs-Created"); len(v) > 0 {
		if t, err := time.Parse(http.TimeFormat, v); err == nil {
			value.Created = t
		}
	}
	if v := response.Header.Get("Ukvs-Last-Read"); len(v) > 0 {
		if t, err := time.Parse(http.TimeFormat, v); err == nil {
			value.LastRead = t
		}
	}
	if v := response.Header.Get("Ukvs-Modified"); len(v) > 0 {
		if t, err := time.Parse(http.TimeFormat, v); err == nil {
			value.LastWrite = t
		}
	}
	if v := response.Header.Get("Ukvs-Reads"); len(v) > 0 {
		if n, err := strconv.ParseUint(v, 10, 64); err == nil {
			value.Reads = n
		}
	}
	if v := response.Header.Get("Ukvs-Writes"); len(v) > 0 {
		if n, err := strconv.ParseUint(v, 10, 64); err == nil {
			value.Writes = n
		}
	}

	value.DataSize = uint32(response.ContentLength)
	if value.DataSize > 0 && !skipData {
		value.Data = make([]byte, value.DataSize)
		response.Body.Read(value.Data)
	}

	return nil
}
