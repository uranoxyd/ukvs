// Copyright 2023 David Ewelt <uranoxyd@gmail.com>
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful, but
//   WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//   General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program. If not, see <http://www.gnu.org/licenses/>.

package ukvs

import (
	"gitlab.com/uranoxyd/uconf"
)

type MemoryStorage struct {
	storage       map[Key]*Value
	configuration *SharedStorageConfiguration

	size int64
}

func NewMemoryStorage() *MemoryStorage {
	return &MemoryStorage{
		storage:       make(map[Key]*Value),
		configuration: &SharedStorageConfiguration{},
	}
}

func (s *MemoryStorage) loadConfig(instruction *uconf.Instruction) (err error) {
	s.configuration.loadConfig(instruction)
	return nil
}

func (s *MemoryStorage) Config() *SharedStorageConfiguration {
	return s.configuration
}

func (c *MemoryStorage) Start() error {
	logInfo("[MemoryStorage] start")
	return nil
}
func (c *MemoryStorage) Stop() error {
	logInfo("[MemoryStorage] stop")
	return nil
}
func (c *MemoryStorage) KeyCount() int {
	km := make(Keymap)
	c.CollectKeys(km)
	return len(km)
}
func (c *MemoryStorage) Keys() (keys []Key) {
	km := make(Keymap)
	c.CollectKeys(km)
	for k := range km {
		keys = append(keys, k)
	}
	return
}
func (c *MemoryStorage) CollectKeys(keymap Keymap) {
	for k := range c.storage {
		keymap[k] = true
	}
	return
}
func (s *MemoryStorage) Has(key Key) bool {
	return s.storage[key] != nil
}
func (c *MemoryStorage) Head(key Key) (*Value, error) {
	return c.Get(key)
}
func (s *MemoryStorage) Get(key Key) (*Value, error) {
	return s.storage[key], nil
}
func (s *MemoryStorage) SetHead(key Key, value *Value) error {
	if storedValue, found := s.storage[key]; found {
		storedValue.ValueHeader = value.ValueHeader
	}
	return nil
}
func (s *MemoryStorage) Set(key Key, value *Value) error {
	if s.storage[key] == value {
		return nil
	}

	if s.storage[key] != nil {
		s.size -= int64(s.storage[key].DataSize)
	}

	s.storage[key] = value

	s.size += int64(value.DataSize)

	return nil
}
func (s *MemoryStorage) Delete(key Key) error {
	if value, ok := s.storage[key]; ok {
		s.size -= int64(value.DataSize)
		delete(s.storage, key)
	}
	return nil
}
func (s *MemoryStorage) Size() int64 {
	return s.size
}
