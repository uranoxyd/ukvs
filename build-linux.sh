#!/bin/bash

#-- ensure the verison file is created
./version.py > /dev/null

branch=$(git rev-parse --abbrev-ref HEAD)
version=$(cat version)

buildPath=builds/linux/$version

echo building for branch $branch with version $version
mkdir -p $buildPath

cp LICENSE README.md $buildPath/

cd app
GOOS=linux go build -o ../$buildPath/ukvs .
cp config.example.conf ../$buildPath/
cd ..

tarName=ukvs-$version.tar.gz
cd $buildPath
rm -f $tarName
tar cfvz $tarName *
shopt -s extglob
rm !($tarName) || true
shopt -u extglob
cd ..

ln -sf $version $branch
