// Copyright 2023 David Ewelt <uranoxyd@gmail.com>
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful, but
//   WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//   General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program. If not, see <http://www.gnu.org/licenses/>.

package ukvs

import (
	"flag"
	"fmt"
	"io"
	"net"
	"net/http"
	"os"
	"strings"

	"gitlab.com/uranoxyd/uconf"
)

type Application struct {
	serveMux         *http.ServeMux
	servers          []*Server
	metricsServer    *MetricsServer
	listenAddressMap map[string]bool
	listeners        []*Listener
	started          bool
}

func NewApplication() (app *Application) {
	app = &Application{
		serveMux:         http.NewServeMux(),
		listenAddressMap: make(map[string]bool),
	}

	app.serveMux.HandleFunc("/", app.httpHandler)

	return
}

func (app *Application) GetServerByRoot(path string) *Server {
	for _, s := range app.servers {
		if strings.HasPrefix(path, s.root) {
			return s
		}
	}
	return nil
}

func (app *Application) loadConfig(scope *uconf.Scope) error {
	for _, instruction := range scope.Instructions() {
		switch instruction.Name() {
		case "listen":
			if len(instruction.Arguments()) < 1 {
				return fmt.Errorf("missing argument: bind address")
			}

			listener := NewListener()
			if err := listener.ParseAddress(instruction.Argument(0).String()); err != nil {
				return err
			}

			app.listeners = append(app.listeners, listener)

		case "server":
			server := NewServer(app.serveMux)
			if err := server.loadConfig(instruction.SubScope()); err != nil {
				return err
			}

			app.servers = append(app.servers, server)

		case "metrics":
			server := NewMetricsServer(app)
			if err := server.loadConfig(instruction); err != nil {
				return err
			}
			app.metricsServer = server
		}
	}

	logDebugf("loaded %d servers", len(app.servers))

	return nil
}

func (app *Application) writeValueHeader(w http.ResponseWriter, server *Server, value *Value) {
	if server.exposedHeadersFlags&ValueHeader_Etag == ValueHeader_Etag {
		// TODO this should be a hash of the data, but it is ok for now
		w.Header().Set("ETag", hashSha1([]byte(value.LastWrite.Format(http.TimeFormat))))
	}
	if server.exposedHeadersFlags&ValueHeader_Created == ValueHeader_Created {
		w.Header().Set("Ukvs-Created", value.Created.Format(http.TimeFormat))
	}
	if server.exposedHeadersFlags&ValueHeader_LastRead == ValueHeader_LastRead {
		w.Header().Set("Ukvs-Last-Read", value.LastRead.Format(http.TimeFormat))
	}
	if server.exposedHeadersFlags&ValueHeader_LastWrite == ValueHeader_LastWrite {
		w.Header().Set("Last-Modified", value.LastWrite.Format(http.TimeFormat))
	}
	if server.exposedHeadersFlags&ValueHeader_Reads == ValueHeader_Reads {
		w.Header().Set("Ukvs-Reads", fmt.Sprint(value.Reads))
	}
	if server.exposedHeadersFlags&ValueHeader_Writes == ValueHeader_Writes {
		w.Header().Set("Ukvs-Writes", fmt.Sprint(value.Writes))
	}
}

func (app *Application) httpHandler(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	//--
	//-- find server for this request path
	//--
	server := app.GetServerByRoot(r.URL.Path)
	if server == nil || !server.started {
		w.WriteHeader(http.StatusNotFound)
		writeLine(w, "Not found")
		return
	}

	//--
	//-- try get the real ip from this request
	//--
	var requestIP net.IP
	if realIPString := r.Header.Get("X-Real-IP"); realIPString != "" {
		if i := strings.LastIndex(realIPString, ":"); i > -1 {
			realIPString = realIPString[:i]
		}
		requestIP = net.ParseIP(realIPString)
	} else {
		if i := strings.LastIndex(r.RemoteAddr, ":"); i > -1 {
			r.RemoteAddr = r.RemoteAddr[:i]
		}
		requestIP = net.ParseIP(r.RemoteAddr)
	}

	// fmt.Printf("requestIP: %v\n", requestIP)

	setResponseStatus := func(code int, message string) {
		server.metrics.CountStatus(code)
		w.WriteHeader(code)
		if len(message) > 0 {
			writeLine(w, message)
		}
	}

	// if !server.allowedMethods[strings.ToUpper(r.Method)] {
	// 	setResponseStatus(http.StatusMethodNotAllowed, "Method not allowed")
	// 	return
	// }

	server.metrics.CountRequest(r)

	//--
	//-- check all excluding conditions
	//--

	// relativePath := r.URL.Path[len(server.root):]
	// fmt.Printf("relativePath: %v\n", relativePath)

	//--
	//-- check host header
	if server.host != "" && r.Host != server.root {
		setResponseStatus(http.StatusNotFound, "Not found")
		return
	}

	// if !server.processConfig(w, r, server.configScope) {
	// 	setResponseStatus(http.StatusNotFound, "Not found")
	// 	return
	// }
	if server.processConfig(w, r, server.configScope) {
		setResponseStatus(http.StatusForbidden, "Forbidden")
		return
	}

	if server.denyAll && !(server.allowAll || server.allowMap.Contains(requestIP)) {
		setResponseStatus(http.StatusForbidden, "Forbidden")
		return
	} else if !server.denyAll && (!server.allowAll || (server.denyMap.Contains(requestIP) && !server.allowMap.Contains(requestIP))) {
		setResponseStatus(http.StatusForbidden, "Forbidden")
		return
	}

	if len(server.auth.users) > 0 {
		if user, pass, ok := r.BasicAuth(); ok {
			if server.auth.users[user] != hashSha1([]byte(pass)) {
				w.Header().Set("WWW-Authenticate", fmt.Sprintf(`Basic realm="%s"`, server.auth.realm))
				setResponseStatus(http.StatusUnauthorized, "Authentication required")
				return
			}
		} else {
			w.Header().Set("WWW-Authenticate", fmt.Sprintf(`Basic realm="%s"`, server.auth.realm))
			setResponseStatus(http.StatusUnauthorized, "Authentication required")
			return
		}
	}

	storageController := server.storage
	if storageController == nil {
		w.WriteHeader(http.StatusInternalServerError)
		setResponseStatus(http.StatusInternalServerError, "Internal server error")
		return
	}

	switch r.Method {
	case "HEAD":
		key := Key(r.URL.Path[1:])
		if storedValue, _ := storageController.Head(key); storedValue != nil {
			w.Header().Set("Content-Length", fmt.Sprint(storedValue.DataSize))
			app.writeValueHeader(w, server, storedValue)
			setResponseStatus(http.StatusOK, "")

		} else {
			setResponseStatus(http.StatusNotFound, "Not found")
		}
		return

	// case "OPTIONS":
	// 	w.Header().Set("Allow", server.allowedMethods.String())
	// 	setResponseStatus(http.StatusOK, "")
	// 	return

	case "PUT":
		key := Key(r.URL.Path[1:])
		if data, err := io.ReadAll(r.Body); err == nil {
			if err := storageController.Set(key, NewValue(data)); err != nil {
				switch err.(type) {
				case *KeySetForbiddenError:
					setResponseStatus(http.StatusForbidden, err.Error())
					return
				}
				setResponseStatus(http.StatusInternalServerError, "Internal server error")
				return
			}
			setResponseStatus(http.StatusOK, "")
		} else {
			setResponseStatus(http.StatusInternalServerError, "Internal server error")
		}
		return

	case "GET":
		key := Key(r.URL.Path[1:])
		if storedValue, _ := storageController.Get(key); storedValue != nil {
			app.writeValueHeader(w, server, storedValue)
			setResponseStatus(http.StatusOK, "")
			w.Write(storedValue.Data)
		} else {
			setResponseStatus(http.StatusNotFound, "Not found")
		}
		return

	case "DELETE":
		key := Key(r.URL.Path[1:])
		if storageController.Delete(key) == nil {
			setResponseStatus(http.StatusOK, "")
		} else {
			setResponseStatus(http.StatusNotFound, "Not found")
		}
		return
	}

	setResponseStatus(http.StatusBadRequest, "Bad request")
}

func (app *Application) Load() error {
	config := flag.String("config", "config.conf", "configuration path")

	configCode, err := os.ReadFile(*config)
	if err != nil {
		return err
	}

	cfgParser := uconf.NewParser()
	cfgScope, err := cfgParser.Parse(string(configCode))
	if err != nil {
		return err
	}

	// cfgScope.Print()

	if err := app.loadConfig(cfgScope); err != nil {
		fmt.Println("error loading config")
		return err
	}

	return nil
}

func (app *Application) Start() (err error) {
	if app.started {
		return nil
	}
	app.started = true

	logInfo("[Application] start")

	for _, listener := range app.listeners {

		if app.listenAddressMap[listener.Address()] {
			continue
		}
		app.listenAddressMap[listener.Address()] = true
		logInfo("[Application] starting listener: ", listener.Address())
		go func(l *Listener) {
			if err = http.ListenAndServe(l.Address(), app.serveMux); err != nil {
				logErrorf("[Application] error starting listener for address: %s", l.Address())
				logError("[Application] ", err)
			}
		}(listener)
	}

	for _, server := range app.servers {
		if err := server.Start(app); err != nil {
			return err
		}
	}

	logDebugf("[Application] started %d servers", len(app.servers))

	if app.metricsServer != nil && app.metricsServer.enabled {
		app.metricsServer.Start()
		logDebugf("[Application] metrics server started")
	}

	return nil
}

func (app *Application) Stop() error {
	if !app.started {
		return nil
	}
	app.started = false

	logInfo("[Application] stop")

	for _, server := range app.servers {
		if err := server.Stop(); err != nil {
			return err
		}
	}

	logDebugf("[Application] stopped %d servers", len(app.servers))

	return nil
}
