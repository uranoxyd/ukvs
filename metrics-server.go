package ukvs

import (
	"fmt"
	"io"
	"net/http"

	"gitlab.com/uranoxyd/uconf"
)

type MetricsServer struct {
	app      *Application
	mux      *http.ServeMux
	listener *Listener
	enabled  bool
	started  bool
}

func NewMetricsServer(app *Application) *MetricsServer {
	s := &MetricsServer{
		app:      app,
		mux:      http.NewServeMux(),
		listener: NewListener(),
	}
	s.mux.HandleFunc("/metrics", s.handleRequest)
	return s
}

func (app *MetricsServer) loadConfig(instruction *uconf.Instruction) (err error) {
	scope := instruction.SubScope()
	if scope == nil {
		return nil
	}

	for _, instruction := range scope.Instructions() {
		switch instruction.Name() {
		case "enabled":
			if len(instruction.Arguments()) < 1 {
				break
			}
			if app.enabled, err = instruction.Argument(0).AsBool(); err != nil {
				return
			}

		case "listen":
			if len(instruction.Arguments()) < 1 {
				break
			}
			if err = app.listener.ParseAddress(instruction.Argument(0).String()); err != nil {
				return
			}
		}
	}
	return nil
}

func (s *MetricsServer) handleRequest(w http.ResponseWriter, req *http.Request) {
	defer req.Body.Close()
	io.WriteString(w, "# HELP ukvs_requests_total Total number of requests\n")
	io.WriteString(w, "# TYPE ukvs_requests_total counter\n")
	var totalRequests uint64
	for _, server := range s.app.servers {
		totalRequests += server.metrics.Requests

		io.WriteString(w, fmt.Sprintf(`ukvs_requests_total{root="%s"} %d`+"\n", server.root, server.metrics.Requests))

		for k, v := range server.metrics.RequestsPerMethod {
			io.WriteString(w, fmt.Sprintf(`ukvs_requests_total{root="%s", method="%s"} %d`+"\n", server.root, k, v))
		}

		for k, v := range server.metrics.RequestStatusCodes {
			io.WriteString(w, fmt.Sprintf(`ukvs_requests_total{root="%s", status="%d"} %d`+"\n", server.root, k, v))
		}
	}
	io.WriteString(w, fmt.Sprintf(`ukvs_requests_total %d`+"\n", totalRequests))
}

func (s *MetricsServer) Start() {
	if s.started {
		return
	}
	s.started = true
	go func() {
		logInfo("starting metrics server under: ", s.listener.Address())
		if err := http.ListenAndServe(s.listener.Address(), s.mux); err != nil {
			logError(err)
		}
		s.started = false
	}()
}
