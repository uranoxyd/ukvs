// Copyright 2023 David Ewelt <uranoxyd@gmail.com>
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful, but
//   WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//   General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program. If not, see <http://www.gnu.org/licenses/>.

package ukvs

import (
	"net"
	"net/http"
	"strings"

	"gitlab.com/uranoxyd/uconf"
)

//--
//-- Server
//--

type Server struct {
	serveMux *http.ServeMux
	storage  *StorageController

	metrics *ServerMetrics

	//-- config
	configScope *uconf.Scope
	root        string
	host        string

	exposedHeadersFlags uint16
	auth                *ServerAuth

	allowMap IPNetMap
	denyMap  IPNetMap
	allowAll bool
	denyAll  bool

	//-- runtime

	started bool
}

func NewServer(serveMux *http.ServeMux) *Server {
	s := &Server{
		serveMux:            serveMux,
		metrics:             NewServerMetrics(),
		exposedHeadersFlags: ValueHeader_Etag | ValueHeader_LastWrite,
		auth:                NewServerAuth(),
		allowMap:            make(IPNetMap),
		denyMap:             make(IPNetMap),
		allowAll:            true,
	}
	return s
}

func (s *Server) Root() string {
	return s.root
}
func (s *Server) Storage() *StorageController {
	return s.storage
}

func (s *Server) checkAllow(ip net.IP) bool {
	return s.allowMap == nil || s.allowMap.Contains(ip)
}
func (s *Server) checkDeny(ip net.IP) bool {
	return s.allowMap == nil || s.allowMap.Contains(ip)
}

func (s *Server) loadConfig(scope *uconf.Scope) error {
	if s.configScope == nil {
		s.configScope = scope
	}

	for _, v := range scope.Instructions() {
		switch v.Name() {
		case "storages":
			if s.storage != nil {
				break
			}
			s.storage = NewStorageController()
			s.storage.chain.loadConfig(v)

		// case "deny-methods":
		// 	for _, m := range v.Arguments() {
		// 		s.allowedMethods.Delete(m.String())
		// 	}
		// case "allow-methods":
		// 	for _, m := range v.Arguments() {
		// 		s.allowedMethods.Set(m.String(), true)
		// 	}

		case "root":
			if len(v.Arguments()) < 1 {
				break
			}

			s.root = v.Argument(0).String()

		case "host":
			if len(v.Arguments()) < 1 {
				break
			}

			s.host = v.Argument(0).String()

		case "exposed-headers":
			var names map[string]uint16 = map[string]uint16{
				"created":    ValueHeader_Created,
				"last-read":  ValueHeader_LastRead,
				"last-write": ValueHeader_LastWrite,
				"reads":      ValueHeader_Reads,
				"writes":     ValueHeader_Writes,
			}

			for _, header := range v.Arguments() {
				value := header.String()
				if len(value) == 0 {
					continue
				}

				if value == "none" {
					s.exposedHeadersFlags = 0
					continue
				} else if value == "all" {
					s.exposedHeadersFlags = 0xFFFF
				}

				operation := value[0]
				var flag string
				if operation != '+' && operation != '-' {
					operation = '+'
					flag = value
				} else {
					flag = value[1:]
				}
				switch operation {
				case '=':
					s.exposedHeadersFlags = names[flag]
				case '+':
					s.exposedHeadersFlags |= names[flag]
				case '-':
					s.exposedHeadersFlags ^= names[flag]
				}
			}

		}
	}

	return nil
}

func (server *Server) processConfig(w http.ResponseWriter, r *http.Request, scope *uconf.Scope) (abort bool) {
	//-- path relative to root
	relativePath := r.URL.Path[len(server.root):]

	//--
	//--
	server.auth = NewServerAuth()
	for _, instruction := range scope.Instructions() {
		switch instruction.Name() {
		case "allow":
			if len(instruction.Arguments()) < 1 {
				break
			}

			if instruction.Argument(0).String() == "all" {
				server.allowAll = true
				break
			}

			if _, ipNet, err := net.ParseCIDR(instruction.Argument(0).String()); err == nil {
				server.allowAll = false
				server.allowMap[ipNet] = true
			}

		case "deny":
			if len(instruction.Arguments()) < 1 {
				break
			}

			if instruction.Argument(0).String() == "all" {
				server.denyAll = true
				break
			}

			if _, ipNet, err := net.ParseCIDR(instruction.Argument(0).String()); err == nil {
				server.denyMap[ipNet] = true
			}

		case "add-header":
			if len(instruction.Arguments()) < 2 {
				break
			}
			w.Header().Add(instruction.Argument(0).String(), instruction.Argument(1).String())

		case "set-header":
			if len(instruction.Arguments()) < 2 {
				break
			}
			w.Header().Set(instruction.Argument(0).String(), instruction.Argument(1).String())

		case "location":
			path := instruction.Argument(0).String()
			if strings.HasPrefix(relativePath, path) && server.processConfig(w, r, instruction.SubScope()) {
				return true
			}

		case "method":
			method := instruction.Argument(0).String()
			if strings.EqualFold(r.Method, method) && server.processConfig(w, r, instruction.SubScope()) {
				return true
			}

		case "auth":
			if instruction.NumArguments() > 0 {
				server.auth.realm = string(instruction.Argument(0))
			}
			if subscope := instruction.SubScope(); subscope != nil {
				for _, iUser := range subscope.Instructions() {
					server.auth.users[iUser.Name()] = string(iUser.Argument(0))
				}
			}

		case "return":
			return true
		}

	}

	return
}

func (s *Server) Start(app *Application) (err error) {
	if s.started {
		return
	}
	s.started = true

	logInfo("[Server] start")

	if s.storage != nil {
		if err := s.storage.Start(); err != nil {
			return err
		}
	}

	return nil
}
func (s *Server) Stop() (err error) {
	if !s.started {
		return
	}
	s.started = false

	logInfo("[Server] stop")

	if s.storage != nil {
		if err := s.storage.Stop(); err != nil {
			return err
		}
	}

	return nil
}
