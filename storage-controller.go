// Copyright 2023 David Ewelt <uranoxyd@gmail.com>
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful, but
//   WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//   General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program. If not, see <http://www.gnu.org/licenses/>.

package ukvs

import (
	"sync"
	"time"
)

type StorageController struct {
	// storages     map[string]Storage
	chain        *StorageChain
	storageMutex sync.Mutex
	stopped      bool
}

func NewStorageController() *StorageController {
	return &StorageController{
		// storages: make(map[string]Storage),
		chain: NewStorageChain(),
	}
}

//--

func (c *StorageController) Chain() *StorageChain {
	return c.chain
}

func (c *StorageController) Start() error {
	c.storageMutex.Lock()
	defer c.storageMutex.Unlock()

	logInfo("[StorageController] start")

	return c.chain.Start()
}

func (c *StorageController) Stop() error {
	c.storageMutex.Lock()
	defer c.storageMutex.Unlock()

	if c.stopped {
		return nil
	}
	c.stopped = true

	logInfo("[StorageController] stop")

	// for _, storage := range c.chain.storages {
	// 	cfg := storage.Config()
	// 	if cfg.deleteAfter > 0 {
	// 		for _, k := range storage.Keys() {

	// 		}
	// 	}
	// }

	return c.chain.Stop()
}

func (c *StorageController) CollectKeys(keymap Keymap) {
	c.storageMutex.Lock()
	defer c.storageMutex.Unlock()

	c.chain.CollectKeys(keymap)
}
func (c *StorageController) Has(key Key) bool {
	c.storageMutex.Lock()
	defer c.storageMutex.Unlock()

	return c.chain.Has(key)
}
func (c *StorageController) Head(key Key) (value *Value, err error) {
	c.storageMutex.Lock()
	defer c.storageMutex.Unlock()

	if value, _ = c.chain.Get(key); value != nil {
		return
	}

	return
}
func (c *StorageController) Get(key Key) (value *Value, err error) {
	c.storageMutex.Lock()
	defer c.storageMutex.Unlock()

	if value, _ = c.chain.Get(key); value != nil {
		value.LastRead = time.Now().UTC()
		value.Reads++
		c.chain.SetHead(key, value)

		return
	}

	return
}
func (c *StorageController) Set(key Key, value *Value) error {

	c.storageMutex.Lock()
	defer c.storageMutex.Unlock()

	for _, storage := range c.chain.storages {
		cfg := storage.Config()
		if cfg.maxItems > 0 && storage.KeyCount()+1 > cfg.maxItems {
			logTrace("[StorageController.Set] max items")
			return NewKeySetForbiddenError("max items reached")
		} else if cfg.maxItemSize > 0 && value.DataSize > uint32(cfg.maxItemSize) {
			logTrace("[StorageController.Set] data to large")
			return NewKeySetForbiddenError("data to large")
		} else if cfg.maxSize > 0 && storage.Size()+int64(value.DataSize) > int64(cfg.maxSize) {
			logTrace("[StorageController.Set] max size reached")
			return NewKeySetForbiddenError("max size reached")
		} else if cfg.writeOnce && c.chain.Has(key) {
			logTrace("[StorageController.Set] write once")
			return NewKeySetForbiddenError("write-once")
		}
	}

	// logTrace("[StorageController.Set] increasing writes")

	value.Writes++
	value.LastWrite = time.Now().UTC()

	//-- set the value in all storages
	for _, storage := range c.chain.storages {
		if err := storage.Set(key, value); err != nil {
			return err
		}
	}

	return nil
}
func (c *StorageController) Delete(key Key) error {
	c.storageMutex.Lock()
	defer c.storageMutex.Unlock()

	c.chain.Delete(key)

	return nil
}
func (c *StorageController) Size() (size int64) {
	c.storageMutex.Lock()
	defer c.storageMutex.Unlock()

	return c.chain.Size()
}
