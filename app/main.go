// Copyright 2023 David Ewelt <uranoxyd@gmail.com>
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful, but
//   WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//   General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program. If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"bufio"
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"

	"gitlab.com/uranoxyd/ukvs"
)

var exitSignalChannel chan os.Signal

func main() {
	if err := main_default(); err != nil {
		fmt.Println("error:", err)
		os.Exit(1)
	}
}

func main_default() (err error) {
	ukvs.SetLogLevel(ukvs.LogLevelTrace)

	app := ukvs.NewApplication()
	if err := app.Load(); err != nil {
		return err
	}
	if err := app.Start(); err != nil {
		return err
	}
	defer app.Stop()

	go func() {
		var selectedServer *ukvs.Server
		var selectedStorage ukvs.Storage

		reader := bufio.NewReader(os.Stdin)
		for {
			if line, _, err := reader.ReadLine(); err == nil {
				lineStr := string(line)
				// fmt.Println("<<<", lineStr)

				var cmd string
				var args []string

				if strings.ContainsRune(lineStr, ' ') {
					tmp := strings.Split(lineStr, " ")
					cmd = tmp[0]
					if len(tmp) > 1 {
						args = tmp[1:]
					}
				} else {
					cmd = lineStr
				}

				switch cmd {
				case "help":
				case "server":
					if len(args) == 0 {
						fmt.Println("expected server root")
						break
					}
					if server := app.GetServerByRoot(args[0]); server != nil {
						selectedServer = server
						fmt.Println("selected server for root:", server.Root())
					} else {
						fmt.Println("server not found:", args[0])
					}

				case "storage":
					if len(args) == 0 {
						fmt.Println("usage: storage <index>")
						break
					}
					if selectedServer == nil {
						fmt.Println("no server selected")
						break
					}

					if index, err := strconv.Atoi(args[0]); err == nil {
						storages := selectedServer.Storage().Chain().Storages()
						if index < 0 || index >= len(storages) {
							fmt.Println("storage index out of bounds")
							break
						}
						selectedStorage = storages[index]
					}

				case "keys":
					if selectedStorage == nil {
						fmt.Println("no storage selected")
						break
					}

					keys := selectedStorage.Keys()
					fmt.Printf("storage has %d keys\n", len(keys))
					for _, k := range keys {
						fmt.Println(" -", k)
					}

				case "get":
					if selectedStorage == nil {
						fmt.Println("no storage selected")
						break
					}
					if len(args) == 0 {
						fmt.Println("usage: get <key>")
						break
					}
					if value, err := selectedStorage.Get(ukvs.Key(args[0])); err == nil {
						fmt.Println(string(value.Data))
					} else {
						fmt.Println("error:", err)
					}
				}
			} else {
				break
			}
		}
	}()

	//-- wait for SIGINT
	exitSignalChannel = make(chan os.Signal, 1)
	signal.Notify(exitSignalChannel, syscall.SIGTERM, syscall.SIGINT)
	signal := <-exitSignalChannel

	if signal == syscall.SIGTERM {
		os.Exit(1)
	} else if signal == syscall.SIGINT {
		os.Exit(1)
	}

	return nil
}
